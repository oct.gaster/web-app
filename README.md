# TIME IN UNIVERSITY

This is a web-application project, which aims to maintain the univeristy<br>
staff timetable. Additionally, it provides access to the scientific and<br>
educational materials created in the particular univeristy (for example, <br>
articles, links to the videolectures recordings, electronic textbooks etc).<br>

In order to use the system, one should have an account. It could be<br>
registered via the __Register__ menu item, in the upper right corner of the home page.<br>

The application can be used either by teachers and students. For instance,<br>
university teachers can publish videorecordings of their online lectures in<br>
the application. Then, these recordings could be downloaded by students or<br>
collected into courses, to be used in massive education.<br>
