class RESTError(Exception):
    def __init__(self, message, http_status_code):
        self.http_status_code = http_status_code
        self.message = message

    def __str__(self):
        return self.message


class RESTSignInError(RESTError):
    pass


class RESTQueryError(RESTError):
    pass


class RESTUnspecifiedTimeError(RESTQueryError):
    pass
