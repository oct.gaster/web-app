from enum import Enum
from time_in_university import db


class PositionEnum(Enum):
    default = 'administrative staff'
    professor = 'professor'
    senior_teacher = 'senior teacher'
    teacher = 'teacher'
    assistant = 'assistant'
    phd_student = 'Ph.D. student'
    msc_student = 'M.Sc. student'
    admin = 'administrative staff'

    @classmethod
    def choices(cls):
        choices = []
        for choice in cls:
            choices.append((choice.name, choice.value))
        return choices


class RankEnum(Enum):
    default = 'none'
    sc_doctor = 'science doctor'
    phd = 'Ph.D.'
    msc = 'M.Sc'

    @classmethod
    def choices(cls):
        choices = []
        for choice in cls:
            choices.append((choice.name, choice.value))
        return choices


class WorkTypeEnum(Enum):
    usual = 'usual'
    videolecture = 'videolecture'
    conference = 'conference'

    @classmethod
    def choices(cls):
        choices = []
        for choice in cls:
            choices.append((choice.name, choice.value))
        return choices
