from flask_login import UserMixin
from time_in_university import db, login_manager
from time_in_university.models import PositionEnum, RankEnum


@login_manager.user_loader
def load_employee(empl_id):
    return Employee.query.get(int(empl_id))


class Employee(db.Model, UserMixin):
    __tablename__ = 'employees'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(31), unique=True, nullable=False)
    password = db.Column(db.String(63), nullable=False)
    name = db.Column(db.String(31), nullable=False)
    surname = db.Column(db.String(31), nullable=False)
    position = db.Column(db.Enum(PositionEnum),
               nullable=False, default=PositionEnum.default)
    rank = db.Column(db.Enum(RankEnum),
           nullable=False, default=RankEnum.default)
    salary = db.Column(db.Integer, nullable=False)
    department_id = db.Column(db.Integer,
                              db.ForeignKey('departments.id'), nullable=False)
    work_time = db.relationship('WorkTime', backref='employee', lazy=True)

    def __repr__(self):
        return f'Employee("{self.name} {self.surname}", "{self.email}", \
"{self.position.value}", "{self.rank.value}", {self.salary}, {self.department_id})'
