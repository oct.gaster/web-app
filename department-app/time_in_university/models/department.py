from time_in_university.models import db


class Department(db.Model):
    __tablename__ = 'departments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(127), nullable=False, unique=True)
    faculty = db.Column(db.String(127), nullable=False)
    location = db.Column(db.String(63), nullable=False)
    employees = db.relationship('Employee', backref='department', lazy=True)

    def __repr__(self):
        return f'Department("{self.name}", "{self.faculty}", "{self.location}")'
