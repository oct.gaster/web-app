from datetime import datetime
from time_in_university.models import db, WorkTypeEnum


class WorkTime(db.Model):
    __tablename__ = 'work_time'

    id = db.Column(db.Integer, primary_key=True)
    empl_id = db.Column(db.Integer, db.ForeignKey('employees.id'), nullable=False)
    location = db.Column(db.String(63))
    start_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    end_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    work_type = db.Column(db.Enum(WorkTypeEnum))
    token = db.Column(db.Text)

    def __repr__(self):
        return f'WorkTime({self.empl_id}, "{self.location}", \
{self.start_time}:{self.end_time}, "{self.work_type}", "{self.token}")'

    def to_dict(self):
        return {
            'location': self.location,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'work_type': self.work_type.value,
            'token': self.token
        }
