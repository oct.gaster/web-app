import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_migrate import Migrate


TIME_FORMAT = '%Y-%m-%d %H:%M:%S'


app = Flask(__name__)
app.config['SECRET_KEY'] = '2d70b2ebe8c9e7ca6306654d39885c08'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'

from time_in_university.views import routes
from time_in_university.rest import endpoints
