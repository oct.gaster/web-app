from datetime import datetime
from flask import render_template, url_for, flash, redirect, request, abort
from flask_login import login_user, current_user, logout_user, login_required
from wtforms.validators import ValidationError
from time_in_university import app, db, bcrypt
from time_in_university.models import PositionEnum, RankEnum
from time_in_university.models.employee import Employee
from time_in_university.models.department import Department
from time_in_university.models.work_time import WorkTime
from .forms import RegistrationForm, LoginForm, UpdateAccountForm
from .forms import FilterEmployeesForm, FilterDepartmentsForm
from .forms import AddWorkTimeForm, FilterWorkTimeForm, AddDepartmentForm
from .forms import ExtFilterWorkTimeForm, department_choices, faculty_choices


@app.route('/')
@app.route('/home')
def home():
    return render_template('index.html',
                  heading='Welcome to the "Time in university" web application')


@app.route('/register', methods=['POST', 'GET'])
def register():
    if current_user.is_authenticated:
        flash('You are logged in')
        return redirect(url_for('home'))
    form = RegistrationForm()
    form.department.choices = department_choices()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        empl = Employee(
        email=form.email.data, name=form.name.data, surname=form.surname.data,
        position=PositionEnum[form.position.data], rank=RankEnum[form.rank.data],
        salary=form.salary.data, department_id=form.department.data, password=hashed_password
        )
        db.session.add(empl)
        db.session.commit()
        flash('Your account was created. You may log in now to the site')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['POST', 'GET'])
def login():
    if current_user.is_authenticated:
        flash('You are logged in')
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        empl = Employee.query.filter_by(email=email).first()
        if empl and bcrypt.check_password_hash(empl.password, password):
            login_user(empl, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash(f'Invalid email or password')
    return render_template('login.html', title='Log in', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/personal_page', methods=['POST', 'GET'])
@login_required
def personal_page():
    full_name = current_user.name + ' ' + current_user.surname
    hours = WorkTime.query.filter_by(empl_id=current_user.id)
    form = FilterWorkTimeForm()
    if form.validate_on_submit():
        if form.start_time.data > form.end_time.data:
            flash('Start time should go before the ending time')
        else:
            hours = hours.filter(
            form.start_time.data <= WorkTime.start_time,
            WorkTime.end_time <= form.end_time.data
            )
            if form.work_type.data != 'any':
                hours = hours.filter(WorkTime.work_type == form.work_type.data)
    hours = hours.order_by(-WorkTime.start_time)
    return render_template('personal_page.html',
       title='Personal page', heading=full_name, hours=hours, form=form)


@app.route('/account', methods=['POST', 'GET'])
@login_required
def account():
    form = UpdateAccountForm()
    form.department.choices = department_choices()
    if form.validate_on_submit():
        current_user.email = form.email.data
        current_user.name = form.name.data
        current_user.surname = form.surname.data
        current_user.position = form.position.data
        current_user.rank = form.rank.data
        current_user.salary = form.salary.data
        current_user.department_id = form.department.data
        db.session.commit()
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.email.data = current_user.email
        form.name.data = current_user.name
        form.surname.data = current_user.surname
        form.position.data = current_user.position.name
        form.rank.data = current_user.rank.name
        form.salary.data = current_user.salary
        form.department.data = current_user.department_id
    return render_template('account.html', title='Account settings', form=form)


@app.route('/add_work_time', methods=['POST', 'GET'])
@login_required
def add_work_time():
    hours = WorkTime.query.filter_by(empl_id=current_user.id)
    form = AddWorkTimeForm()
    if form.validate_on_submit():
        work_time_intersect = False
        reversed_time = False
        uncompleted_hour = False
        if form.start_time.data > form.end_time.data:
            flash('Start time must be before the ending time')
            reversed_time = True
        if form.end_time.data > datetime.utcnow():
            flash('Only completed working hours are allowed')
            uncompleted_hour = True
        for hour in hours:
            if hour.start_time <= form.start_time.data <= hour.end_time \
             or hour.start_time <= form.end_time.data <= hour.end_time:
                flash('There are intersecting work hours')
                work_time_intersect = True
                break
        if not (work_time_intersect or reversed_time or uncompleted_hour):
            hour = WorkTime(
                empl_id=current_user.id,
                location=form.location.data,
                start_time=form.start_time.data,
                end_time=form.end_time.data,
                work_type=form.work_type.data,
                token=form.token.data
            )
            db.session.add(hour)
            db.session.commit()
            flash('Your working time was successfully submitted')
            return redirect(url_for('personal_page'))
    return render_template('add_work_time.html', title='Add work time', form=form)


@app.route('/employees', methods=['POST', 'GET'])
def show_employees():
    form = FilterEmployeesForm()
    form.department.choices = [(-1, 'any'),] + department_choices()
    if form.validate_on_submit():
        employees = Employee.query.filter(
            Employee.salary.between(form.min_salary.data, form.max_salary.data)
        )
        if form.name_suffix.data:
            employees = employees.filter(Employee.name.like('%' + form.name_suffix.data + '%'))
        if form.surname_suffix.data:
            employees = employees.filter(Employee.surname.like('%' + form.surname_suffix.data + '%'))
        if form.email.data:
            employees = employees.filter(Employee.email == form.email.data)
        if form.department.data != -1:
            employees = employees.filter(Employee.department_id == form.department.data)
        if form.position.data != 'any':
            employees = employees.filter(Employee.position == form.position.data)
        if form.rank.data != 'any':
            employees = employees.filter(Employee.rank == form.rank.data)
    else:
        employees = Employee.query.order_by(Employee.surname).all()
    return render_template('employees.html',
           title='University employees', employees=employees, form=form)


@app.route('/departments', methods=['POST', 'GET'])
def show_departments():
    form = FilterDepartmentsForm()
    form.faculty.choices = faculty_choices()
    departments = Department.query.order_by(Department.faculty)
    if form.validate_on_submit():
        if form.faculty.data != 'any':
            departments = Department.query.filter(Department.faculty == form.faculty.data)
        if form.name_suffix.data:
            departments = departments.filter(Department.name.like('%' + form.name_suffix.data + '%'))
    return render_template('departments.html',
           title='Deparments', departments=departments, form=form)


@app.route('/add_department', methods=['POST', 'GET'])
@login_required
def add_department():
    form = AddDepartmentForm()
    if form.validate_on_submit():
        print('Hello, world!')
        dep = Department(name=form.name.data,
                         faculty=form.faculty.data, location=form.location.data)
        db.session.add(dep)
        db.session.commit()
        return redirect(url_for('add_department'))
    return render_template('add_department.html',
                           title='Add new department', form=form)


@app.route('/work_time', methods=['POST', 'GET'])
@login_required
def show_work_time():
    employees = Employee.query.all()
    form = ExtFilterWorkTimeForm()
    form.employee.choices = [(-1, 'any'),] + \
                 [(e.id, e.name + ' ' + e.surname) for e in employees]
    hours = WorkTime.query.filter()
    if form.validate_on_submit():
        if form.start_time.data > form.end_time.data:
            flash('Start time must be before end time')
        else:
            hours = hours.filter(
                form.start_time.data <= WorkTime.start_time,
                WorkTime.end_time <= form.end_time.data
            )
        print(form.employee.data)
        if form.employee.data != -1:
            hours = hours.filter(WorkTime.empl_id == form.employee.data)
            print('employee filtering')
        if form.work_type.data != 'any':
            hours = hours.filter(WorkTime.work_type == form.work_type.data)
    return render_template('work_time.html',
            title='Work time records for all employees', form=form, hours=hours)


@app.route('/work_time/<int:hour_id>/update', methods=['POST', 'GET'])
@login_required
def edit_work_time(hour_id):
    hour = WorkTime.query.get_or_404(hour_id)
    if hour.empl_id != current_user.id:
        abort(403)
    form = AddWorkTimeForm()
    if form.validate_on_submit():
        if form.start_time.data > form.end_time.data:
            flash('Start time must be before end time')
        elif form.end_time.data > datetime.utcnow():
            flash('Only completed working hours are allowed')
        else:
            hour.location = form.location.data
            hour.start_time = form.start_time.data
            hour.end_time = form.end_time.data
            hour.work_type = form.work_type.data
            hour.token = form.token.data
            db.session.commit()
            return redirect(url_for('personal_page'))
    elif request.method == 'GET':
        form.location.data = hour.location
        form.start_time.data = hour.start_time
        form.end_time.data = hour.end_time
        form.work_type.data = hour.work_type.name
        form.token.data = hour.token
    return render_template('add_work_time.html', title='Edit work time record', form=form)


@app.route('/work_time/<int:hour_id>/delete', methods=['POST',])
@login_required
def delete_work_time(hour_id):
    hour = WorkTime.query.get_or_404(hour_id)
    if hour.empl_id != current_user.id:
        abort(403)
    db.session.delete(hour)
    db.session.commit()
    return redirect(url_for('personal_page'))
