from datetime import datetime
from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import StringField, SelectField, PasswordField, SubmitField
from wtforms import BooleanField, IntegerField, DateTimeField, FormField
from wtforms.validators import DataRequired, Length, Email
from wtforms.validators import EqualTo, NumberRange, ValidationError
from time_in_university import TIME_FORMAT
from time_in_university.models import PositionEnum, RankEnum, WorkTypeEnum
from time_in_university.models.department import Department
from time_in_university.models.employee import Employee


def department_choices():
    departments = Department.query.all()
    return [(dep.id, dep.name) for dep in departments]


def faculty_choices():
    f_choices = {'any',}
    departments = Department.query.all()
    for dep in departments:
        f_choices.add(dep.faculty)
    return list(f_choices)


class RegistrationForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    name = StringField('Name', validators=[DataRequired(), Length(max=20)])
    surname = StringField('Surname', validators=[DataRequired(), Length(max=20)])
    position = SelectField('Position', choices=PositionEnum.choices())
    rank = SelectField('Rank', choices=RankEnum.choices())
    salary = IntegerField('Salary')
    department = SelectField('Department',
                choices=[(0, 'default'),],
                coerce=int)
    password = PasswordField('Password', validators=[DataRequired(),])
    confirm_password = PasswordField('Confirm password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register an employee')

    def validate_email(self, email):
        empl = Employee.query.filter_by(email=email.data).first()
        if empl:
            raise ValidationError('The employee with the same email already exists')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(),])
    remember = BooleanField('Remember me')
    submit = SubmitField('Log in')


class UpdateAccountForm(FlaskForm):
    email = StringField('Email',
            validators=[DataRequired(), Email()])
    name = StringField('Name',
            validators=[DataRequired(), Length(max=20)])
    surname = StringField('Surname',
            validators=[DataRequired(), Length(max=20)])
    position = SelectField('Position',
            choices=PositionEnum.choices())
    rank = SelectField('Rank', choices=RankEnum.choices())
    salary = IntegerField('Salary')
    department = SelectField('Department',
                 choices=[(0, 'default'),], coerce=int)
    submit = SubmitField('Update')

    def validate_email(self, email):
        if current_user.email != email.data:
            empl = Employee.query.filter_by(email=email.data).first()
            if empl:
                raise ValidationError('The employee with the same email already exists')


class WorkTimeForm(FlaskForm):
    start_time = DateTimeField('Start time (' + TIME_FORMAT + ')',
                 default=datetime.utcnow, validators=[DataRequired(),])
    end_time = DateTimeField('End time (' + TIME_FORMAT + ')',
                 default=datetime.utcnow, validators=[DataRequired(),])

    def validate_start_time(self, start_time):
        if start_time.data > datetime.utcnow():
            raise ValidationError('Work hour can not start after the present moment')


class AddWorkTimeForm(WorkTimeForm):
    location = StringField('Location')
    work_type = SelectField('Work type', choices=WorkTypeEnum.choices())
    token = StringField('Token')
    submit = SubmitField('Submit')


class FilterWorkTimeForm(WorkTimeForm):
    work_type = SelectField('Work type', choices=[('any', 'any'),] + WorkTypeEnum.choices())
    submit = SubmitField('Filter')


class ExtFilterWorkTimeForm(FilterWorkTimeForm):
    employee = SelectField('Employee', choices=[(-1, 'any'),], coerce=int)


class FilterEmployeesForm(FlaskForm):
    name_suffix = StringField('Name')
    surname_suffix = StringField('Surname')
    email = StringField('Email')
    min_salary = IntegerField('Minimal salary',
                 validators=[NumberRange(min=0),], default=0)
    max_salary = IntegerField('Maximal salary',
                 validators=[NumberRange(min=0),], default=0)
    department = SelectField('Department',
                 choices=[(-1, 'any')] + [(0, 'default'),],
                 coerce=int)
    position = SelectField('Position', choices=[('any', 'any'),] + PositionEnum.choices())
    rank = SelectField('Rank', choices=[('any', 'any'),] + RankEnum.choices())
    submit = SubmitField('Find employees')


class AddDepartmentForm(FlaskForm):
    name = StringField('Department name', validators=[DataRequired(),])
    faculty = StringField('Faculty name', validators=[DataRequired(),])
    location = StringField('Deparment location', validators=[DataRequired(),])
    submit = SubmitField('Add new department')

    def validate_name(self, name):
        dep = Department.query.filter_by(name=name.data).first()
        if dep:
            raise ValidationError('Department with the same name already exists')


class FilterDepartmentsForm(FlaskForm):
    name_suffix = StringField('Department name')
    faculty = SelectField('Faculty', choices=list({'any',}))
    submit = SubmitField('Find departments')
