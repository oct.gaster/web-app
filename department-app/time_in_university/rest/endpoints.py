from datetime import datetime
from flask import render_template, request, redirect, jsonify
from flask_api import status
from time_in_university import app, bcrypt, db, TIME_FORMAT
from time_in_university.exceptions import RESTError, RESTQueryError
from time_in_university.exceptions import RESTSignInError, RESTUnspecifiedTimeError
from time_in_university.models import WorkTypeEnum
from time_in_university.models.work_time import WorkTime
from time_in_university.models.employee import Employee


INVALID_CONTENT_TYPE = 'Invalid request content type. JSON expected'
INVALID_CREDENTIALS = 'Invalid credentials'
INCONSISTENT_TIME = 'Inverse or inconsistent time parameters order detected'
ID_NOT_GIVEN = 'WorkTime record id was expected'
NO_RECORDS_FOUND = 'No records found'
UNSPECIFIED_TIME = 'start_time or end_time are unspecified'
NO_WORK_TIME_ID_FOUND = 'No WorkTime record with such ID found'
ACCESS_NOT_ALLOWED = 'Access is not allowed'


def sign_in():
    """
    This method checks credentials supplied in the request JSON, and if they
    are wrong, the exception is raised. Otherwise, the Employee object is
    retrieved from the DB and returned
    """
    email = request.json.get('email')
    password = request.json.get('password')
    if not (email and password):
        raise RESTSignInError(INVALID_CREDENTIALS, status.HTTP_401_UNAUTHORIZED)
    employee = Employee.query.filter_by(email=email).first()
    if employee and bcrypt.check_password_hash(employee.password, password):
        return employee
    else:
        raise RESTSignInError(INVALID_CREDENTIALS, status.HTTP_401_UNAUTHORIZED)


def is_choice_enum_member(choice_value, enum_class):
    for choice in enum_class:
        if choice.value == choice_value:
            return choice
    return None


def str_to_datetime(string):
    if string:
        string = datetime.strptime(string, TIME_FORMAT)
    return string


def get_start_end_time():
    """
    This method retrieves start_time and end_time from the request JSON.
    It checks that the parameters time format is
    correct (according to the TIME_FORMAT variable), and also that
    start_time is not in future, so preventing redundant queries to the DB.
    As a result, both time parameters are returned as datetime objects or Nones,
    if some of them is absent in the request JSON
    """
    start_time = str_to_datetime(request.json.get('start_time'))
    end_time = str_to_datetime(request.json.get('end_time'))
    if start_time \
       and ((end_time and start_time > end_time) \
            or start_time > datetime.utcnow()):
        raise RESTQueryError(INCONSISTENT_TIME, status.HTTP_406_NOT_ACCEPTABLE)
    return start_time, end_time


@app.route('/rest/work_time/delete', methods=['DELETE',])
def delete_work_time_record():
    """
    This view deletes WorkTime record with the specified ID
    """
    if request.content_type != 'application/json':
        return jsonify({'message': INVALID_CONTENT_TYPE}), status.HTTP_400_BAD_REQUEST
    try:
        employee = sign_in()
    except RESTError as err:
        return jsonify({'message': str(err)}), err.http_status_code
    id = request.json.get('id')
    if not id:
        return jsonify({'message': ID_NOT_GIVEN}), \
               status.HTTP_406_NOT_ACCEPTABLE
    hour = WorkTime.query.get_or_404(int(id), description=NO_WORK_TIME_ID_FOUND)
    if hour.empl_id != employee.id:
        return jsonify({'message': ACCESS_NOT_ALLOWED}), status.HTTP_403_FORBIDDEN
    db.session.delete(hour)
    db.session.commit()
    return jsonify(''), status.HTTP_204_NO_CONTENT


@app.route('/rest/work_time/update', methods=['PUT',])
def update_work_time():
    """
    This view updates the existing work time record by its id. Only employees
    who created the record are able to change it
    """
    if request.content_type != 'application/json':
        return jsonify({'message': INVALID_CONTENT_TYPE}), status.HTTP_400_BAD_REQUEST
    try:
        employee = sign_in()
        start_time, end_time = get_start_end_time()
    except RESTError as err:
        return jsonify({'message': str(err)}), err.http_status_code
    id = request.json.get('id')
    if not id:
        return jsonify({'message': ID_NOT_GIVEN}), \
               status.HTTP_406_NOT_ACCEPTABLE
    hour = WorkTime.query.get_or_404(id, description=NO_WORK_TIME_ID_FOUND)
    if hour.empl_id != employee.id:
        return jsonify({'message': ACCESS_NOT_ALLOWED}), \
               status.HTTP_403_FORBIDDEN
    if start_time:
        hour.start_time = start_time
    if end_time:
        hour.end_time = end_time
    location = request.json.get('location')
    work_type = request.json.get('work_type')
    token = request.json.get('token')
    if location:
        hour.location = location
    choice = is_choice_enum_member(work_type, WorkTypeEnum)
    if choice:
        hour.work_type = choice
    if token:
        hour.token = token
    db.session.commit()
    return jsonify({
        'id': hour.id,
        'location': hour.location,
        'start_time': hour.start_time,
        'end_time': hour.end_time,
        'work_type': hour.work_type.value,
        'token': hour.token
    }), status.HTTP_200_OK


@app.route('/rest/work_time/submit', methods=['PUT',])
def submit_work_time():
    """
    This view inserts new work time record into the database, assigning
    this record to the employee who sent the request
    """
    if request.content_type != 'application/json':
        return jsonify({'message': INVALID_CONTENT_TYPE}), status.HTTP_400_BAD_REQUEST
    try:
        employee = sign_in()
        start_time, end_time = get_start_end_time()
    except RESTError as err:
        return jsonify({'message': str(err)}), err.http_status_code
    except ValueError as err:
        return jsonify({'message': 'Time conversion failed: ' + str(err)}), \
               status.HTTP_406_NOT_ACCEPTABLE
    if not (start_time and end_time):
        return jsonify({'message': UNSPECIFIED_TIME}), status.HTTP_406_NOT_ACCEPTABLE
    if end_time > datetime.utcnow():
        return jsonify({'message': 'Unfinished work hours are not allowed'}), \
               status.HTTP_406_NOT_ACCEPTABLE
    # check that work period under submission does not intersects with any
    # already submitted period:
    hours = WorkTime.query.filter_by(empl_id=employee.id)
    start_intersect = hours.filter(
        WorkTime.start_time < start_time,
        start_time < WorkTime.end_time
    )
    end_intersect = hours.filter(
        WorkTime.start_time < end_time,
        end_time < WorkTime.end_time
    )
    if start_intersect.first() or end_intersect.first():
        return jsonify({'message': 'There are intersecting work hours'}), \
               status.HTTP_406_NOT_ACCEPTABLE
    location = request.json.get('location')
    if not location:
        location = ''
    work_type = request.json.get('work_type')
    choice = is_choice_enum_member(work_type, WorkTypeEnum)
    if not (work_type and choice):
        return jsonify({'message': 'Unknown work type'}), status.HTTP_406_NOT_ACCEPTABLE
    token = request.json.get('token')
    if not token:
        token = ''
    hour = WorkTime(
        empl_id=employee.id,
        location=location,
        start_time=start_time,
        end_time=end_time,
        work_type=choice,
        token=token
    )
    db.session.add(hour)
    db.session.commit()
    return jsonify({
        'location': hour.location,
        'start_time': hour.start_time,
        'end_time': hour.end_time,
        'work_type': hour.work_type.value,
        'token': hour.token
    }), status.HTTP_201_CREATED


@app.route('/rest/work_time/all', methods=['POST',])
def get_work_time_all():
    """
    This view returns list of the working hours of all employees, after the
    credentials check
    """
    if request.content_type != 'application/json':
        return jsonify({'message': INVALID_CONTENT_TYPE}), status.HTTP_400_BAD_REQUEST
    try:
        employee = sign_in()
    except RESTSignInError as err:
        return jsonify({'message': str(err)}), err.http_status_code
    response = []
    for hour in WorkTime.query.all():
        e = Employee.query.get(hour.empl_id)
        response.append({
            'id': hour.id,
            'employee': e.name + ' ' + e.surname if e else 'missing',
            'location': hour.location,
            'start_time': hour.start_time,
            'end_time': hour.end_time,
            'work_type': hour.work_type.value,
            'token': hour.token
        })
    if not len(response):
        return jsonify({'message': 'No WorkTime records found'}), status.HTTP_404_NOT_FOUND
    return jsonify(response), status.HTTP_200_OK


@app.route('/rest/work_time/filter', methods=['POST',])
def get_work_time_filter():
    """
    This view returns list of all working hours of all employees, where items
    obey constraints specified in the request, after the credentials check
    """
    if request.content_type != 'application/json':
        return jsonify({'message': INVALID_CONTENT_TYPE}), status.HTTP_400_BAD_REQUEST
    try:
        employee = sign_in()
        start_time, end_time = get_start_end_time()
    except RESTError as err:
        return jsonify({'message': str(err)}), err.http_status_code
    except ValueError as err:
        return jsonify({'message': 'Time conversion failed: ' + str(err)}), \
               status.HTTP_406_NOT_ACCEPTABLE
    id = request.json.get('id')
    if id:
        hour = WorkTime.query.get_or_404(int(id), description=NO_WORK_TIME_ID_FOUND)
        e = Employee.query.get(hour.empl_id)
        return jsonify({
            'id': id,
            'employee': e.name + ' ' + e.surname if e else 'missing',
            'location': hour.location,
            'start_time': hour.start_time,
            'end_time': hour.end_time,
            'work_type': hour.work_type.value,
            'token': hour.token
        }), status.HTTP_200_OK
    hours = WorkTime.query.filter()
    if start_time:
        hours = hours.filter(WorkTime.start_time >= start_time)
    if end_time:
        hours = hours.filter(WorkTime.end_time <= end_time)
    work_type = request.json.get('work_type')
    if work_type:
        choice = is_choice_enum_member(work_type, WorkTypeEnum)
        if not choice:
            return jsonify({'message': 'Unknown work type'}), status.HTTP_406_NOT_ACCEPTABLE
        hours = hours.filter(WorkTime.work_type == choice)
    email_to_find = request.json.get('email_to_find')
    if email_to_find:
        e = Employee.query.filter_by(email=email_to_find).first_or_404('No emails found')
        hours = hours.filter(WorkTime.empl_id == e.id)
    if hours.first():
        response = []
        for hour in hours:
            if not email_to_find:
                e = Employee.query.get(hour.empl_id)
            response.append({
                'id': hour.id,
                'employee': e.name + ' ' + e.surname if e else 'missing',
                'location': hour.location,
                'start_time': hour.start_time,
                'end_time': hour.end_time,
                'work_type': hour.work_type.value,
                'token': hour.token
            })
        return jsonify(response), status.HTTP_200_OK
    return jsonify({'message': 'No WorkTime records for such constraints found'}), \
           status.HTTP_404_NOT_FOUND
