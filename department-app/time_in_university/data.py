employees_list = [
    {
    'name': 'Julius',
    'surname': 'Schwinger',
    'email': 'julius.schwinger@email.com',
    'position': 'professor',
    'rank': 'science doctor',
    'salary': 3000,
    'department': 'Theoretical physics'
    },
    {
    'name': 'Marie',
    'surname': 'Curie',
    'email': 'marie.curie@email.com',
    'position': 'professor',
    'rank': 'Ph.D.',
    'salary': 3000,
    'department': 'Physics of atom and nuclei'
    },
    {
    'name': 'Wolfgang',
    'surname': 'Pauli',
    'email': 'wolfgang.pauli@email.com',
    'position': 'senior teacher',
    'rank': 'Ph.D.',
    'salary': 2000,
    'department': 'Theoretical physics'
    },
    {
    'name': 'Peter',
    'surname': 'Kapitza',
    'email': 'peter.kapitza@email.com',
    'position': 'Ph.D. student',
    'rank': 'M.Sc.',
    'salary': 1000,
    'department': 'Physics of ultra-low temperatures'
    },
    {
    'name': 'Immanuel',
    'surname': 'Kant',
    'email': 'immanuel.kant@email.com',
    'position': 'senior teacher',
    'rank': 'science doctor',
    'salary': 2500,
    'department': 'Philosophy'
    },
    {
    'name': 'James',
    'surname': 'Watson',
    'email': 'james.watson@email.com',
    'position': 'assistant',
    'rank': 'science doctor',
    'salary': 3500,
    'department': 'Molecular biology'
    },
    {
    'name': 'Julius',
    'surname': 'Caesar',
    'email': 'julius.caesar@email.com',
    'position': 'administrative staff',
    'rank': 'Ph.D.',
    'salary': 2500,
    'department': 'Rectorate'
    }
]

departments_list = [
    {
    'name': 'Theoretical physics',
    'faculty': 'Physics and mathematics',
    'location': '48.434444, 35.034750'
    },
    {
    'name': 'Physics of the ultra-low temperatures',
    'faculty': 'Physics and mathematics',
    'location': '48.434444, 35.034750'
    },
    {
    'name': 'Physics of atom and nuclei',
    'faculty': 'Physics and mathematics',
    'location': '48.434444, 35.034750'
    },
    {
    'name': 'Molecular biology',
    'faculty': 'Biology',
    'location': '-22.926430, -43.477205'
    },
    {
    'name': 'Philosophy',
    'faculty': 'Humanitarian sciences, arts and languages',
    'location': '40.668559, 22.856551'
    },
    {
    'name': 'Rectorate',
    'faculty': 'Administration',
    'location': '50.339500, 30.480944'
    }
]
