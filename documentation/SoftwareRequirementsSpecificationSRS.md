#Time in university
##Vision
This is a Python web application, which maintains the
timetable of the university staff. The possibility of online
education was also taken into account.


Application should provide:


  * Storing of the working hours of the university personnel
  * Storing data about the personnel and departments
  * Display the timetable of the particular person for a given period of time
  * Adding/deleting records about working hours of the personnel
  * Adding/deleting records about workers
  * Display salary of one worker for the given period of time
  * Display salary statistics for a department(s)
  * Display information about worker
  * Filter displayed workers by departments, personal information


##1. Employees
This mode displays information about university employees. It is possible to
filter data by department, position, rank, and to sort with respect
to salary.
##2. Departments
This mode displays information about departments. Filtering by faculty is
present.
##3. Hours
This mode shows work hours of the university employees. Search by the
employee name and surname is present. Filter by the work type is present.
The mode displays hours spent in the specified period of time.
