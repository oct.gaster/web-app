#Database structure in the "Time in university" application
List of tables in the database and their structure
  1. Employees
     * Email (unique field, which is used to log in into system)
     * Password
     * Name
     * Surname
     * Position (professor, senior teacher, teacher, assistant, Ph.D. student,
       M.Sc. student, administrative staff)
     * Rank (science doctor, Ph.D., M.Sc.)
     * Salary
     * Department ID (inf -> 1 _Departments_)
  2. Departments
     * Name
     * Faculty
     * Location
  3. Hours
     * Employee ID (1 -> 1 _Employees_)
     * Location
     * Starting time
     * Ending time
     * Type of the work (usual, conference)
     * Token (this may be link to the videolecture recording,
       to the repository with documents or other possible
       work results)
