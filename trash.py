@app.route('/work_time', methods=['POST', 'GET'])
def show_work_time():
    form = FilterWorkTimeForm()
    employees = [
    (empl.id, empl.name + ' ' + empl.surname) for empl in Employee.query.all()
    ]
    form.employee.choices = [(-1, 'any'),] + employees
    hours = WorkTime.query.all()
    if form.validate_on_submit():
        if form.start_time.data > form.end_time.data:
            flash('Start time should go before the ending time')
        else:
            hours = WorkTime.query.filter(WorkTime.start_time>=form.start_time.data)\
            .filter(WorkTime.end_time<=form.end_time.data)
            if form.employee.data != -1:
                hours = hours.filter(WorkTime.empl_id==form.empl.data)
            if form.work_type.data != 'any':
                hours = hours.filter(WorkTime.work_type==form.work_type.data)
            hours = hours.order_by(-WorkTime.start_time)
    return render_template('work_time.html', form=form, hours=hours)
